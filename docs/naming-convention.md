---
sidebar_position: 2
---

# Adlandırma Kuralları

## Componentler & Hooklar

Genel olarak componentler için *PascalCase* isimlendirme ve hook/functions için ise *camelCase* kullanılsada projemizde **kebab-case kullanılmalıdır**.

Bunun sebebi MacOS işletim sistemlerinin varsayılan olarak case-insensitive dosya sistemi kullanmasıdır.

Refactor yapılırken <mark>myComponent.js</mark> dosyasının <mark>MyComponent.js</mark> şeklinde değişitirilmesi Git'i tetiklemeyecektir ve bu CI sisteminin hata vermesine neden olacaktır.

Bu yapı Next.js ve Angular'ın tavsiye edilen isimlendirme kuralı olarak da kullanılmaktadır.

### İlgili bağlantılar:

[Next.js CMS Example](https://github.com/vercel/next.js/tree/canary/examples/cms-wordpress/components)

[Angular Coding Style Guide](https://angular.io/guide/styleguide#separate-file-names-with-dots-and-dashes)

