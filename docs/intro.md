---
sidebar_position: 1
---

# Klasör Yapısı

Tür odaklı klasör yapısı küçük ölçekli projelere daha uygun olduğundan bu proje için özellik odaklı **(feature driven)** klasör yapısı kullanılmıştır.

Aşağıda örnek yapı belirtilmiştir.

```
├── public
└── src
   ├── constants
   ├── features
   |  ├── auth
   |  |  ├── forgot-password.js
   |  |  ├── index.js
   |  |  ├── login-view.js
   |  |  └── singup-view.js
   |  ├── cart
   |  |  ├── index.js
   |  |  ├── item
   |  |  └── mini-view
   |  ├── checkout
   |  ├── product
   |  |  ├── card
   |  |  |  ├── card-box.js
   |  |  |  ├── card-box.module.scss
   |  |  |  └── index.js
   |  |  └── slider
   |  ├── seo
   |  ├── ui
   |  |  ├── button
   |  |  ├── checkbox
   |  |  ├── footer
   |  |  ├── grid
   |  |  |  ├── grid.js
   |  |  |  ├── grid.module.scss
   |  |  |  └── index.js
   |  |  ├── header
   |  |  ├── hero
   |  |  |  └── slider
   |  |  |     ├── hero-slider.js
   |  |  |     ├── hero-slider.module.scss
   |  |  |     └── index.js
   |  |  ├── icons
   |  |  |  └── index.js
   |  |  ├── index.js
   |  |  ├── input
   |  |  ├── layout
   |  |  ├── modal
   |  |  ├── radio
   |  |  ├── skeleton
   |  |  └── slider
   |  |     ├── collection
   |  |     └── hero
   |  └── users
   |     ├── index.js
   |     ├── login
   |     ├── signup
   |     └── use-auth.js
   ├── pages
   |  ├── 404.js
   |  ├── api
   |  ├── index.js
   |  ├── sepet.js
   |  ├── siparisler.js
   |  ├── _app.js
   |  └── _document.js
   └── styles
      └── main.scss
```

Bob Martin'in makalesinden alıntı [**Screaming Architecture**](https://blog.cleancoder.com/uncle-bob/2011/09/30/Screaming-Architecture.html)

> Your architectures should tell readers about the system, not about the frameworks you used in your system. If you are building a health-care system, then when new programmers look at the source repository, their first impression should be: “Oh, this is a heath-care system”. Those new programmers should be able to learn all the use cases of the system, and still not know how the system is delivered. They may come to you and say: “We see some things that look sorta like models, but where are the views and controllers”, and you should say: “Oh, those are details that needn’t concern you at the moment, we’ll show them to you later.”
